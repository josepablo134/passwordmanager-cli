#! /usr/bin/python3
import argparse
import sys
import sqlite3
import commands

from src.pwdManagerModel import User
from src.pwdManagerModel import Password

helpMsg = """
	%s -u[USER] -p[PASS] [MODE]	[OPTIONS]
	OPTIONS : 
		--pwd-id	:	get by password name
	MODE : 
		-C	Create a new password object, opens a form
			--json	:	Reads the new data from stdin in json format

		-R	Read a password object, prints a json object

		-U	Update a password object, opens a form
			--json	:	Reads the new data from stdin in json format

		-D	Delete a password object, opens a confirmation
			--noconfirm	:	Force operation
	EXAMPLES :
	#	Create a new password object from json
	cat passwordObj | CMD -uUSER -pPASS -C --json
	#	Update a new password object from json
	cat passwordObj | CMD -uUSER -pPASS -U --pwd-id hotmailPersonal --json
	#	Create a new password object from prompt
	CMD -uUSER -pPASS -C
		passwordName:
		user:
		password:
		url:
		comment:
"""
helpMsg = helpMsg%sys.argv[0]

parser = argparse.ArgumentParser()

parser.add_argument( "-u" , help = "User param" )
parser.add_argument( "-p" , help = "Password param" )
parser.add_argument( "-C" , help = "Create a new password object" , action="store_true" )
parser.add_argument( "-R" , help = "Read a password object" , action="store_true" )
parser.add_argument( "-U" , help = "Update a password object" , action="store_true" )
parser.add_argument( "-D" , help = "Delete a password object" , action="store_true" )
parser.add_argument( "-L" , help = "List all password names" , action="store_true" )

parser.add_argument( "--pwd-id" , help = "Search by password nade" , action="store" )
parser.add_argument( "--json" , help = "Load configuration from stdin" , action="store_true" )
parser.add_argument( "--noconfirm" , help = "No confirm nothing" , action="store_true" )

args = parser.parse_args()

db = sqlite3.connect( 'src/devel.db' )
cursor = db.cursor()
user = None
key = None

def done():
	db.commit()
	db.close()

###
#	Load user and key
###
if(not args.u ):
	user = commands.createUserObject()
	user.cur = cursor
	user.save()
elif(not args.p ):
	user,key = commands.getUserFromPassword( args.u , cursor )
else:
	user = commands.getUser( args.u , args.p , cursor )
	key = args.p
#print( "\n\tUser configuration\n" )
#print( user )

passwordQuery = Password( cursor )

##
#	Create new password object
##
if( args.L ):
	commands.showPasswordNames( user.id , cursor , key )
elif( args.C ):
	if( args.json ):
		commands.createPasswordJSON( user.id , cursor , key )
	else:
		commands.createPasswordPrompt( user.id , cursor , key )
elif( args.R ):
	if( args.pwd_id ):
		commands.readPasswordID( args.pwd_id , cursor , key )
	else:
		print("No password id to search")
elif( args.U ):
	if( args.pwd_id ):
		if( args.json ):
			commands.updatePasswordJSON( args.pwd_id , cursor , key , args.noconfirm )
		else:
			commands.updatePasswordPrompt( args.pwd_id , cursor , key , args.noconfirm )
	else:
		print("No password id to search")
elif( args.D ):
	if( args.pwd_id ):
		commands.removePasswordPrompt( args.pwd_id , cursor , key , args.noconfirm )
	else:
		print("No password id to search")

done()

