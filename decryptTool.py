from src.EZEncryptor import Encryptor
import argparse

parser = argparse.ArgumentParser()
parser.add_argument( "-i" , help = "Read from stdin", action="store_true" )
parser.add_argument( "-m" , help = "Message to unlock" )
parser.add_argument( "-p" , help = "Password to unlock" )

args = parser.parse_args()
cerr = lambda m : print("[ERROR] ",m)
msg = ""
if( args.i ):
    if( args.m ):
        msg = args.m
    else:
        msg = input()
else:
    if( not args.m ):
        cerr("No message detected")
        exit(-1)
    else:
        msg = args.m
if( not args.p ):
    cerr("No password detected")
    exit(-1)
enc = Encryptor( args.p )
print( enc.decrypt( msg ) )
