import sys
import json
import argparse
import getpass
from src.EZEncryptor import Encryptor,sha256

parser = argparse.ArgumentParser()
parser.add_argument( "-i" , help = "Read from stdin" , action="store_true" )
parser.add_argument( "-f" , help = "Read from file" )
parser.add_argument( "-p" , help = "Password to unlock file" )
parser.add_argument( "-o" , help = "Store result in output file" )
parser.add_argument( "-u" , help = "User name" )
parser.add_argument( "--pwd-id" , help = "Password name" , action="store" )
parser.add_argument( "--less" , help = "Just show password" , action="store_true" )

cerr = lambda m : print("[ERROR] ",m)
args = parser.parse_args()
jdata = ""
password = ""

if( not args.p ):
    password = getpass.getpass()
else:
    password = args.p
if( len(password) != 16 ):
    cerr("Invalid key")
    exit(-1)
if( args.i ):
    jdata = json.load( sys.stdin )
elif( args.f ):
    with open( args.f ) as json_file:
        jdata = json.load( json_file )
        json_file.close()
else:
    cerr("File not detected")
    exit(-1)

def getUserByPassword( data : str , password : str ):
    passSha = sha256( password )
    for user in data["Users"]:
        if( user["password"] == passSha ):
            return user
def getPasswordsByUserID( data : str , userID : int ):
    pwdArray = []
    for pwd in data["Passwords"]:
        if( pwd["user_id"] == userID ):
            pwdArray.append(pwd)
    return pwdArray
def getPasswordsByName( data : str , name : str ):
    passArray = []
    for pwd in data["Passwords"]:
        if( pwd["name"] == name ):
            passArray.append( pwd )
    return passArray

def unlockPasswords( pwdArray , key : str ):
    enc = Encryptor( key )
    pwdArrayUnlocked = []
    for pwd in pwdArray:
        pwd["password"] = enc.decrypt( pwd["password"] )
        pwd["user"] = enc.decrypt( pwd["user"] )
        pwd["url"] = enc.decrypt( pwd["url"] )
        pwd["comment"] = enc.decrypt( pwd["comment"] )
        pwdArrayUnlocked.append( pwd )
    return pwdArrayUnlocked

user = getUserByPassword( jdata , password )

passArrayU = None
if( args.pwd_id ):
    passArray = getPasswordsByName( jdata , args.pwd_id )
    passArrayU = unlockPasswords( passArray , password )
else:
    passArray = getPasswordsByUserID( jdata , user["id"] )
    passArrayU = unlockPasswords ( passArray , password )

if( args.o ):
    with open( args.o , "w+" ) as json_file:
        json_file.write( json.dumps( passArrayU ) )
        json_file.close()
else:
    if( args.less ):
        for pwd in passArrayU:
            print( pwd["password"] )
    else:
        print( passArrayU )


