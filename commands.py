import getpass
import sqlite3
import json
import sys
from src.pwdManagerModel import User
from src.pwdManagerModel import Password
from src.EZEncryptor import Encryptor
from src.EZEncryptor import sha256

cerr = lambda msg: print( "[ERROR] %s"%msg )

def userPrompt():
	user = input("user name : ")
	password = getpass.getpass()
	print("Please confirm . . .")
	password2 = getpass.getpass()
	comment = input("comment : ")
	if( password != password2 ):
		cerr("Password error")
		exit(-1)
	if( password == '' or user == '' ):
		cerr("Invalid user data")
		exit(-1)
	if( len(password) < 16 or len(password) > 16 ):
		cerr("Invalid password, must be 16 chars long")
		exit( -1 )
	return user,password,comment
def getUserPasswordPrompt():
	password = getpass.getpass()
	if( password == '' ):
		err("Password error")
		exit(-1)
	return password
def createUserObject():
	user = User()
	user.name,password,user.comment = userPrompt()
	user.pass_sha = sha256( password )
	return user

def getUserFromPassword( userID : int , cursor ):
	user = User( cursor )
	password = getUserPasswordPrompt()
	user.getByName( userID )
	if( not user.id ):
		cerr("User error")
		exit(-1)
	if( user.pass_sha != sha256( password ) ):
		cerr("Incorrect password")
		exit(-1)
	return user,password
def getUser( name : str , password : str , cursor ):
	user = User( cursor )
	user.getByName( name )
	if( not user.id ):
		cer("User error")
		exit(-1)
	if( user.pass_sha != sha256( password ) ):
		cerr("Incorrect password")
		exit(-1)
	return user

def createPasswordJSON( userID : int , cursor , key : str):
	jdata = None
	try:
		jdata = json.load( sys.stdin )
	except:
		cerr("JSON invalid")
		exit(-1)
	enc = Encryptor( key )
	password = Password( cursor )
	password.table_id = userID
	try:
		password.name = jdata['name']
		password.pass_sha = enc.encrypt( jdata['password'] )
		password.user = enc.encrypt( jdata['user'] )
		password.url = enc.encrypt( jdata['url'] )
		password.comment = enc.encrypt( jdata['comment'] )
	except:
		cerr("JSON invalid")
		exit(-1)
	password.save()
	print( password )
	print( "\tPassword saved" )

def createPasswordPrompt( userID : int , cursor , key : str ):
	passUserName = input("User name : ")
	passSecret = getpass.getpass()
	print("Please confirm . . .")
	passSecret2 = getpass.getpass()
	if( passSecret != passSecret2 ):
		cerr("Password does not match")
		exit(-1)
	passName = input("Password name : ")
	passURL = input("Password url : ")
	passComment = input("Comment (optional) : ")
	if( passName == "" or
		passURL == "" or
		passSecret == "" or
		passUserName == ""):
		cerr("Data incomplete ")
		exit(-1)
	password = Password( cursor )
	enc = Encryptor( key )
	password.name = passName
	password.pass_sha = enc.encrypt( passSecret )
	password.user = enc.encrypt( passUserName )
	password.url = enc.encrypt( passURL )
	password.comment = enc.encrypt( passComment )
	password.table_id = userID
	password.save()
	print( password )
	print( "\tPassword saved" )
	
def readPasswordID( passID : str , cursor , key : str ):
	enc = Encryptor( key )
	password = Password( cursor )
	if( not password.getByName( passID ) ):
		cerr("Password name not found")
		exit(-1)
	password.pass_sha = enc.decrypt( password.pass_sha )
	password.user = enc.decrypt( password.user )
	password.url = enc.decrypt( password.url )
	password.comment = enc.decrypt( password.comment )
	#print( "\n\tActual configuration\n" )
	print( password.user )
	print( password.url )
	print( password.pass_sha )
	#print( "\n\n" )
	
def updatePasswordPrompt( passID : str , cursor , key : str , confirmation : bool ):
	password = Password( cursor )
	password.getByName ( passID )
	if(not password.id):
		cerr("Password name not found")
		exit(-1)
	enc = Encryptor( key )
	
	password.user = enc.decrypt( password.user )
	password.pass_sha = enc.decrypt( password.pass_sha )
	password.url = enc.decrypt( password.url )
	password.comment = enc.decrypt( password.comment )
	print( "\n\tActual configuration\n" )
	print( password )
	
	tempUser = input("User name : ")
	tempPass = getpass.getpass()
	print("Please confirm . . .")
	tempPass2 = getpass.getpass()
	if( tempPass != tempPass2 ):
		cerr("Password does not match")
		exit(-1)
	tempName = input("Password name : ")
	tempURL = input("Password url : ")
	tempComment = input("Comment (optional) : ")
	if( tempName == "" or
		tempURL == "" or
		tempPass == "" or
		tempUser == ""):
		cerr("Data incomplete ")
		exit(-1)
	if( not confirmation ):
		conf = input("Are you sure you want to edit this password (N/y)").lower()
		if( conf == "y" ):
			pass
		else:
			print("\n\tCanceled")
			return
	password.name = tempName
	password.pass_sha = enc.encrypt( tempPass )
	password.user = enc.encrypt( tempUser )
	password.url = enc.encrypt( tempURL )
	password.comment = enc.encrypt( tempComment )
	password.save()
	print("\t\nPassword updated\n")
def updatePasswordJSON( passID : str , cursor , key : str , confirmation : bool ):
	password = Password( cursor )
	password.getByName ( passID )
	if(not password.id):
		cerr("Password name not found")
		exit(-1)
	enc = Encryptor( key )
	password.user = enc.decrypt( password.user )
	password.pass_sha = enc.decrypt( password.pass_sha )
	password.url = enc.decrypt( password.url )
	password.comment = enc.decrypt( password.comment )
	print( "\n\tActual configuration\n" )
	print( password )
	if( not confirmation ):
		conf = input("Are you sure you want to edit this password (N/y)").lower()
		if( conf == "y" ):
			pass
		else:
			print("\n\tCanceled")
			return
	jdata = None
	try:
		jdata = json.load( sys.stdin )
	except:
		cerr("JSON invalid")
		exit(-1)
	try:
		password.name = jdata['name']
		password.pass_sha = enc.encrypt( jdata['password'] )
		password.user = enc.encrypt( jdata['user'] )
		password.url = enc.encrypt( jdata['url'] )
		password.comment = enc.encrypt( jdata['comment'] )
	except:
		cerr("JSON invalid")
		exit(-1)
	password.save()
	print( password )
	print("Password updated")

def removePasswordPrompt( passID : str , cursor , key : str , confirmation : bool ):
	password = Password( cursor )
	password.getByName ( passID )
	if(not password.id ):
		cerr("Password name not found")
		exit(-1)
	passwordCopy = Password( )
	enc = Encryptor( key )

	passwordCopy.id = password.id
	passwordCopy.table_id = password.table_id
	passwordCopy.name = password.name
	passwordCopy.user = enc.decrypt( password.user )
	passwordCopy.pass_sha = enc.decrypt( password.pass_sha )
	passwordCopy.url = enc.decrypt( password.url )
	passwordCopy.comment = enc.decrypt( password.comment )
	print( passwordCopy )

	if( confirmation ):
		password.remove()
		print("\n\tPassword deleted")
	else:
		conf = input("Are you sure you want to delete this password (N/y)").lower()
		if( conf == "y" ):
			password.remove()
			print("\n\tPassword deleted")
			return
		print("Canceled")

def showPasswordNames( userID : int , cursor , key : str ):
	password = Password( cursor )
	passwordArray = password.getByUserId( userID )
	print("\nPassword Names:\n")
	if( passwordArray ):
		for k in passwordArray:
			print("\t", k.name )
		print( "\tTotal : %d"%len(passwordArray) )
	else:
		print("\tEmpty list")

