import sqlite3

class User:
	"""User class"""
	def __init__(self, cur=None ):
		"""Constructor"""
		self.id=0
		self.name=""
		self.comment=""
		self.pass_sha=""
		if( cur ):
			self.cur = cur
		else:
			self.cur = None
	def __str__(self):
		"""JSON representation"""
		selfDescription = "{ id : %d,\n name : \"%s\",\n comment : \"%s\",\n pass_sha : \"%s\" }"
		selfDescription = selfDescription%( self.id , self.name , self.comment , self.pass_sha )
		return selfDescription
	def save( self ):
		"""Save this class as a new row or update the actual row"""
		if( self.name == "" or
			self.pass_sha == "" ):
			raise Exception("All parameters except ID must be set")
		if( self.cur == None ):
			raise Exception("There is not a SQLite cursor set")
		if( self.id != 0 ):
			return self.__update()
		self.id = self.cur.execute(
			"INSERT INTO User (NAME,COMMENT,PASS_SHA) VALUES (:name,:comment,:pass_sha)",
			{"name":self.name, "comment":self.comment,"pass_sha":self.pass_sha}).lastrowid
		return self.id
	def __update(self):
		"""Once the class content is valid, this method updates the actual row content"""
		self.cur.execute(
			"UPDATE User SET NAME=:name,COMMENT=:comment,PASS_SHA:=pass_sha WHERE ID=:id",
			{"name":self.name, "comment":self.comment,"pass_sha":self.pass_sha,"id":self.id})
		return self.id
	def getByName(self , name : str ):
		"""Get a new element and overwrite this class"""
		if( self.cur == None ):
			raise Exception("There is not a SQLite cursor set")
		u = self.cur.execute(
			"SELECT * FROM User WHERE NAME=:nameID",
			{ "nameID":name }).fetchone()
		if( not u ):
			return None
		self.id = u[0]
		self.name = u[1]
		self.comment = u[2]
		self.pass_sha = u[3]
		return u
	def getById(self , ID : int ):
		"""Get a new element and overwrite this class"""
		if( self.cur == None ):
			raise Exception("There is not a SQLite cursor set")
		u = self.cur.execute(
			"SELECT * FROM User WHERE ID=:ID ",
			{ "ID":ID }).fetchone()
		if( not u ):
			return None
		self.id = u[0]
		self.name = u[1]
		self.comment = u[2]
		self.pass_sha = u[3]
		return u
	def remove( self ):
		"""Remove actual row from DB"""
		if( self.id == 0 ):
			raise Exception("ID is not set")
		self.cur.execute(
			"DELETE FROM User WHERE ID=:id",
			{"id":self.id})
		self.id = 0

class Password:
	"""Password Model"""
	def __init__(self, cur=None ):
		self.id=0
		self.table_id=0
		self.name=""
		self.pass_sha=""
		self.user=""
		self.url=""
		self.comment=""
		if( cur ):
			self.cur = cur
		else:
			self.cur = None
	def __str__(self):
		selfDescription = "{ id : %d,\n table_id : %d,\n name : \"%s\",\n"
		selfDescription += " user : \"%s\",\n pass_sha : \"%s\",\n url : \"%s\",\n comment : \"%s\" }"
		selfDescription = selfDescription%( self.id , self.table_id , self.name , self.user ,
							self.pass_sha , self.url , self.comment )
		return selfDescription
	def save( self ):
		"""Save this class as a new row or update the actual row"""
		if( self.name == "" or
			self.pass_sha == "" or
			self.table_id == 0 or
			self.user == "" or
			self.url == "" ):
			raise Exception("All parameters except ID must be set")
		if( self.cur == None ):
			raise Exception("There is not a SQLite cursor set")
		if( self.id != 0 ):
			return self.__update()
		command = "INSERT INTO Passwords (TABLE_ID,IDNAME,PASS,USER,URL,COMMENT) "
		command += "VALUES (:tableid , :name , :pass_sha , :user , :url , :comment)"

		self.id = self.cur.execute( command ,
			{"name":self.name, "comment":self.comment,"pass_sha":self.pass_sha,
			"tableid":self.table_id,"url":self.url,"user":self.user}).lastrowid
		return self.id
	def __update(self):
		"""Once the class content is valid, this method updates the actual row content"""
		command = "UPDATE Passwords SET TABLE_ID=:tableid , IDNAME=:name , PASS=:pass_sha , "
		command += "USER=:user , URL=:url , COMMENT=:comment WHERE ID=:id"
		self.cur.execute(
			command,
			{"name":self.name, "comment":self.comment,"pass_sha":self.pass_sha,
			"tableid":self.table_id,"url":self.url,"user":self.user,"id":self.id})
		return self.id
	def getByName(self , name : str ):
		"""Get a new element and overwrite this class"""
		if( self.cur == None ):
			raise Exception("There is not a SQLite cursor set")
		u = self.cur.execute(
			"SELECT * FROM Passwords WHERE IDNAME=:nameID ",
			{ "nameID":name }).fetchone()
		if( not u ):
			return None
		self.id= u[0]
		self.table_id= u[1]
		self.name= u[2]
		self.pass_sha= u[3]
		self.user= u[4]
		self.url= u[5]
		self.comment= u[6]
		return u
	def getById(self , ID : int ):
		"""Get a new element and overwrite this class"""
		if( self.cur == None ):
			raise Exception("There is not a SQLite cursor set")
		u = self.cur.execute(
			"SELECT * FROM Passwords WHERE ID=:ID ",
			{ "ID":ID }).fetchone()
		if( not u ):
			return None
		self.id= u[0]
		self.table_id= u[1]
		self.name= u[2]
		self.pass_sha= u[3]
		self.user= u[4]
		self.url= u[5]
		self.comment= u[6]
		return u
	def getByUserId(self, ID : int):
		"""Get a new element and overwrite this class"""
		if( self.cur == None ):
			raise Exception("There is not a SQLite cursor set")
		data = self.cur.execute(
			"SELECT * FROM Passwords WHERE TABLE_ID=:ID ",
			{ "ID":ID }).fetchall()
		if( not data ):
			return None
		Pass = []
		for u in data:
			p = Password()
			p.id= u[0]
			p.table_id= u[1]
			p.name= u[2]
			p.pass_sha= u[3]
			p.user= u[4]
			p.url= u[5]
			p.comment= u[6]
			Pass.append( p )
		return Pass
	def remove( self ):
		"""Remove actual row from DB"""
		if( self.id == 0 ):
			raise Exception("ID is not set")
		self.cur.execute(
			"DELETE FROM Passwords WHERE ID=:id",
			{"id":self.id})
		self.id = 0

