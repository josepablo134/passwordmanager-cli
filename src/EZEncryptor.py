from Crypto import Random
from Crypto.Cipher import AES
import hashlib

class Encryptor:
	def __init__(self, key : str ):
		self.key = key.encode()
	def pad(self, s):
		return s + b"\0" * (AES.block_size - len(s) % AES.block_size)
	def encrypt(self, message : str, key_size=256):
		message = self.pad( message.encode() )
		iv = Random.new().read(AES.block_size)
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return (iv + cipher.encrypt(message)).hex()
	def decrypt(self, ciphertext : str ):
		ciphertext = bytes.fromhex( ciphertext )
		iv = ciphertext[:AES.block_size]
		cipher = AES.new( self.key , AES.MODE_CBC, iv )
		plaintext = cipher.decrypt(ciphertext[AES.block_size:])
		return plaintext.rstrip(b"\0").decode("utf-8")

def sha256( message ):
	sha_signature = hashlib.sha256( message.encode() ).hexdigest()
	return sha_signature


